function eGTokenService($q, $window, $injector) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.localStorage.token) {
                config.headers['Authorization'] = "Bearer " + $window.localStorage.token;
            }
            return config || $q.when(config);
        },

        response: function (response) {
            return response || $q.when(response);
        },

        responseError: function (response) {

            // Session has expired
	        var data = response.data;
            if(data && data.error && data.error.refreshed_token) {

                var $http = $injector.get('$http');
                var deferred = $q.defer();

                $window.localStorage.token = data.error.refreshed_token;
                response.config.headers['Authorization'] = "Bearer " + $window.localStorage.token;

                // When the session recovered, make the same backend call again and chain the request
                //return deferred.promise.then(function() {
                return $http(response.config);
                //});

            }

            return $q.reject(response);
        }
    };
}

angular.module("eGToken", []).factory("eGTokenService", eGTokenService);
